<?php

require_once("env.php");

$req = $db->prepare('SELECT * FROM blogroll_links ORDER BY RAND() LIMIT 1');
$req->execute();
$url = $req->fetch();

if($url && sizeof($url)){
    echo($url['link']);
    header('Location:'. $url["link"]);
    exit(1);
}
else{
    ?>
    There was an unknown error.
    <?php
}